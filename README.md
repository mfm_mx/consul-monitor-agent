# consul-monitor-agent #

## Description

Hashicorp's Consul xinetd monitor agent. This component is designed to return
HTTP response codes and additional information through xinetd to report the current
state of a Consul agent.

This component provides Layer7 (httpcheck) checkups to help load balancers (like HAProxy) decide whether to route messages to backend instances or not.

## Dependencies ##

* Xinetd properly installed
* TCP Port 9001 open

## Configuration ##

* Place the file **consulStatus.sh** on a folder and assign execution permissions:

> chmod +x consulStatus.sh

* Edit **consultStatus.sh** file and update the **CONSUL_CONFIG_DIR** variable.

* Create a new xinetd service:

> sudo nano /etc/xinetd.d/consul-status

* Add the following content to the new xinetd service configuration:

```
service consul_monitor
{
        flags = REUSE
        socket_type = stream
        port = 9001
        wait = no
        user = nobody
        server = [ABSOLUTE_PATH_TO_FOLDER]/consulStatus.sh
        log_on_failure += USERID
        disable = no
        type = UNLISTED
}
```

* Restart xinetd service:

> sudo /etc/init.d/xinetd restart

## Changelog ##

| VERSION       | DESCRIPTION  |
|:-------------:|:-------------|
|1.0.0| Initial Version|
