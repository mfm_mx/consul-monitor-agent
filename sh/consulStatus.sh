#!/bin/bash
CONSUL_CONFIG_DIR="/home/app-admin/scripts/"

IFS=':'
address=""
while read -r key value
do
#	echo $key
	if [[ "$key" == *"bind_addr"* ]];
	then
		address="$(echo -e "${value}" | tr -d '[:space:],\",\,' )"
	fi
done < ${CONSUL_CONFIG_DIR}/consul-config.json

IFS=''

address='http://'${address}':8500'

servers=-1

if [ "$address" != "" ]
then
	IFS='='
	while read -r key value
	do
		if [[ "$key" == *"known_servers"*  ]];
		then
			servers=$(echo -e $value | tr -d '[:space:]')
		fi
	done <<< `consul info -http-addr=${address}`
fi

if [ $servers -gt 0 ]
then
	# Consul agent alive
        jsonResponse="{'service-available':'true','consul-agent-status':'active'}"
        jsonSize=${#jsonResponse}
        jsonSize=$((jsonSize + 2))
	echo -en "HTTP/1.1 200 OK\r\n"
        echo -en "Content-Type: application/json\r\n"
        echo -en "Connection: close\r\n"
        echo -en "Content-Length: $jsonSize\r\n"
        echo -en "\r\n"
        echo -en "$jsonResponse\r\n"
	exit 0
elif [ $servers -eq 0 ]
then
	# Consul agent alive but not joined"
        jsonResponse="{'service-available':'false','consul-agent-status':'unjoined'}"
        jsonSize=${#jsonResponse}
        jsonSize=$((jsonSize + 2))
        echo -en "HTTP/1.1 503 Service Unavailable\r\n"
        echo -en "Content-Type: application/json\r\n"
        echo -en "Connection: close\r\n"
        echo -en "Content-Length: $jsonSize\r\n"
        echo -en "\r\n"
        echo -en "$jsonResponse\r\n"
	exit 1
else
	# Consul agent offline
	jsonResponse="{'service-available':'false','consul-agent-status':'unknown'}"
	jsonSize=${#jsonResponse}
	jsonSize=$((jsonSize + 2))
	echo -en  "HTTP/1.1 503 Service Unavailable\r\n"
	echo -en "Content-Type: application/json\r\n"
	echo -en "Connection: close\r\n"
	echo -en "Content-Length: $jsonSize\r\n"
	echo -en "\r\n"
	echo -en "$jsonResponse\r\n"
	exit 1
fi
